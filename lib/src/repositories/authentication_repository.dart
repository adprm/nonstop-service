import 'package:logging/logging.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/login_response.dart';
import '../utils/http_helper.dart';
import '../utils/preferences_key.dart';

class AuthenticationRepository {
  final Logger _logger = Logger('AuthenticationRepository');

  /// Authenticate a user.
  /// [url] can either be a `String` or a `Uri`.
  ///
  /// returns `void`
  /// save the credentials using [SharedPreferences]
  Future<bool> authenticate(
    dynamic url, {
    @required String phone,
  }) async {
    try {
      Map<String, dynamic> body = await HttpHelper.invokeHttp(
        url,
        RequestType.post,
        body: {'phone': phone},
      );

      _logger.finest('Login Success');

      LoginResponse loginResponse = LoginResponse.fromJson(body);

      _saveCredentials(token: loginResponse.apiToken);

      return true;
    } catch (e) {
      _logger.warning('Couldn\'t login', e.toString());
      rethrow;
    }
  }

  /// Register a new user.
  /// [url] can either be a `String` or a `Uri`.
  ///
  /// returns `void`
  Future<bool> register(
    dynamic url, {
    @required String type,
    @required String fullName,
    @required String email,
    @required String phone,
    @required String provinceId,
    @required String cityId,
    @required String districtId,
    @required String postcode,
    @required String address,
  }) async {
    try {
      Map<String, dynamic> body = await HttpHelper.invokeHttp(
        url,
        RequestType.post,
        body: {
          'type': type,
          'fullname': fullName,
          'email': email,
          'status': 'PENDING',
          'phone': phone,
          'province_id': provinceId,
          'city_id': cityId,
          'district_id': districtId,
          'postcode': postcode,
          'address': address,
        },
      );

      _logger.finest('Register Success');

      return true;
    } catch (e) {
      rethrow;
    }
  }

  void _saveCredentials({
    @required String token,
  }) async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();

      await Future.wait([
        preferences.setString(PreferencesKey.tokenKey, token),
        preferences.setBool(PreferencesKey.isAuthenticatedKey, true),
      ]);

      _logger.finest('Credentials successfully stored.');
    } catch (error) {
      _logger.warning('Credentials could not be stored.');
    }
  }
}
