import 'package:meta/meta.dart';

class APIException implements Exception {
  final int statusCode;
  final bool success;
  final String message;

  APIException({
    @required this.statusCode,
    @required this.success,
    @required this.message,
  });
}
