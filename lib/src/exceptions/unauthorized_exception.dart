import 'package:meta/meta.dart';

class UnauthorizedException implements Exception {
  final int statusCode;
  final bool success;
  final String message;

  UnauthorizedException({
    @required this.statusCode,
    @required this.success,
    @required this.message,
  });
}
