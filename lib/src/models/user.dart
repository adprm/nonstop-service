import 'package:meta/meta.dart';

class UserResponse {
  UserResponse({
    @required this.statusCode,
    @required this.success,
    @required this.message,
    @required this.data,
  });

  final int statusCode;
  final bool success;
  final String message;
  final User data;

  factory UserResponse.fromJson(Map<String, dynamic> json) => UserResponse(
        statusCode: json['status_code'],
        success: json['success'],
        message: json['message'],
        data: User.fromJson(json['data']),
      );

  Map<String, dynamic> toJson() => {
        'status_code': statusCode,
        'success': success,
        'message': message,
        'data': data.toJson(),
      };
}

class User {
  User({
    @required this.id,
    @required this.type,
    @required this.fullName,
    @required this.description,
    @required this.email,
    @required this.phone,
    @required this.apiToken,
    @required this.smsVerify,
    @required this.status,
    @required this.createdAt,
    @required this.updatedAt,
  });

  final int id;
  final String type;
  final String fullName;
  final String description;
  final String email;
  final String phone;
  final String apiToken;
  final String smsVerify;
  final String status;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      type: json['type'],
      fullName: json['fullname'],
      description: json['description'],
      email: json['email'],
      phone: json['phone'],
      apiToken: json['api_token'],
      smsVerify: json['sms_verify'],
      status: json['status'],
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'type': type,
      'fullname': fullName,
      'description': description,
      'email': email,
      'phone': phone,
      'api_token': apiToken,
      'sms_verify': smsVerify,
      'status': status,
      'created_at': createdAt.toIso8601String(),
      'updated_at': updatedAt.toIso8601String(),
    };
  }
}
