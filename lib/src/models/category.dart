import 'package:meta/meta.dart';

class CategoryResponse {
  CategoryResponse({
    @required this.statusCode,
    @required this.success,
    @required this.message,
    @required this.data,
  });

  final int statusCode;
  final bool success;
  final String message;
  final List<Category> data;

  factory CategoryResponse.fromJson(Map<String, dynamic> json) {
    return CategoryResponse(
      statusCode: json['status_code'],
      success: json['success'],
      message: json['message'],
      data: List<Category>.from(json['data'].map((x) => Category.fromJson(x))),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status_code': statusCode,
      'success': success,
      'message': message,
      'data': List<dynamic>.from(data.map((x) => x.toJson())),
    };
  }
}

class Category {
  Category({
    @required this.id,
    @required this.judul,
    @required this.createDate,
  });

  final int id;
  final String judul;
  final DateTime createDate;

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      id: json['id'],
      judul: json['judul'],
      createDate: DateTime.parse(json['create_date']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'judul': judul,
      'create_date': createDate.toIso8601String(),
    };
  }
}
