import 'package:meta/meta.dart';

class ProvinsiResponse {
  ProvinsiResponse({
    @required this.provinsi,
  });

  final List<Provinsi> provinsi;

  factory ProvinsiResponse.fromJson(Map<String, dynamic> json) {
    return ProvinsiResponse(
      provinsi: List<Provinsi>.from(
        json['provinsi'].map((x) => Provinsi.fromJson(x)),
      ),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'provinsi': List<dynamic>.from(provinsi.map((x) => x.toJson())),
    };
  }
}

class Provinsi {
  Provinsi({
    @required this.id,
    @required this.nama,
  });

  final int id;
  final String nama;

  factory Provinsi.fromJson(Map<String, dynamic> json) {
    return Provinsi(
      id: json['id'],
      nama: json['nama'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nama': nama,
    };
  }
}
