import 'package:meta/meta.dart';

class KecamatanResponse {
  KecamatanResponse({
    @required this.kecamatan,
  });

  final List<Kecamatan> kecamatan;

  factory KecamatanResponse.fromJson(Map<String, dynamic> json) {
    return KecamatanResponse(
      kecamatan: List<Kecamatan>.from(
        json['kecamatan'].map((x) => Kecamatan.fromJson(x)),
      ),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'kecamatan': List<dynamic>.from(kecamatan.map((x) => x.toJson())),
    };
  }
}

class Kecamatan {
  Kecamatan({
    @required this.id,
    @required this.idKota,
    @required this.nama,
  });

  final int id;
  final String idKota;
  final String nama;

  factory Kecamatan.fromJson(Map<String, dynamic> json) {
    return Kecamatan(
      id: json['id'],
      idKota: json['id_kota'],
      nama: json['nama'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'id_kota': idKota,
      'nama': nama,
    };
  }
}
